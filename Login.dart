
import 'dart:ffi';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_application_1/Login.dart';
import 'package:flutter/src/material/floating_action_button.dart';

class Login extends StatelessWidget {
  const Login({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text("Login Screen"),
        ),
        body: 
        SingleChildScrollView(
          child: Column(children: [
            SizedBox(
              height: 115,
              width: 115,
              child: Stack(),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: const TextField(
                  obscureText: true,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Username',
                  )),
            ),
        
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: const TextField(
                obscureText: true,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'Password',
                ),
              ),
            ),
        
            
            ElevatedButton(
                onPressed: () => {Navigator.pushNamed(context, '/screen2')},
                child: const Text("Register a Account")),
            SizedBox(height: 20, width: 20, child: Stack()),
            FloatingActionButton(
              onPressed: () => {Navigator.pushNamed(context, '/screen3')},
              child: const Text("Login"),
              
            )
          ]),
        ));
  }
}




class Registration extends StatelessWidget {
  const Registration({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text("Registration Screen"),
        ),
        body: SingleChildScrollView(
          child: Column(children: [
            SizedBox(
              height: 115,
              width: 115,
              child: Stack(),
            ),
            const TextField(
                obscureText: true,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'Email',
                )),
        
            const TextField(
                obscureText: true,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'Username',
                )),
            const TextField(
              obscureText: true,
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                labelText: 'Password',
              ),
            ),
            ElevatedButton(
                onPressed: () => {},
                child: const Text("Account Details")),
                SizedBox(
                  height: 20,
                  width: 20,
                  child: Stack()
                ),
            FloatingActionButton(
              onPressed: () => {Navigator.pushNamed(context, '/screen3')},
              child: const Text("Login"),
            
            )
          ]),
        ));
  }
}


class Dashboard extends StatelessWidget {
  const Dashboard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            title: const Text("Dashboard page")),
        body: Center(
            child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
              ElevatedButton(
                  child: Text("Tap for FeatureScreen 1"),
                  onPressed: () {
                    Navigator.pushNamed(context, '/screen4');
                  }),
              SizedBox(
                height: 10,
              ),
              ElevatedButton(
                  child: Text("Tap for FeatureScreen 2"),
                  onPressed: () {
                    Navigator.pushNamed(context, '/screen5');
                  })
            ])),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            Navigator.pushNamed(context, '/screen6');
          },child: const Text("Profile"),
        ));
  }
}
